import { useState, useContext, useEffect } from 'react';
import { Form, Button, Container } from 'react-bootstrap';
import UserContext from '../../UserContext';
import usersData from '../../data/users';
import Router from 'next/router';
import Head from 'next/head'

export default function index(){
	const {user, setUser} =  useContext(UserContext)

	const [email, setEmail] = useState('')
	const [password, setPassword] = useState('')

	useEffect(() => {
		console.log(`User with email: ${user.email} is an admin: ${user.isAdmin}`);
	}, [user.isAdmin, user.email])

	function authenticate(e){
		e.preventDefault();

		const match = usersData.find(userData => {
			return(userData.email === email && userData.password === password)
		})

		if(match){
			localStorage.setItem('email', email)
			localStorage.setItem('isAdmin', match.isAdmin)

			setUser({
				email: localStorage.getItem('email'),
				isAdmin: match.isAdmin
			})

			alert('Successfully logged in.')
			Router.push('/courses')
		}else{
			alert('Authentication failed.')
		}
	}

	return (
		<React.Fragment>	
		<Head>
			<title>Log In</title>
		</Head>
		<Container>
			<Form onSubmit={(e) => authenticate(e)}>
				<Form.Group controlId="userEmail">
					<Form.Label></Form.Label>
					<Form.Control type="email" placeholder="Enter your email" value={email} onChange={(e) => setEmail(e.target.value)} required />
				</Form.Group>

				<Form.Group controlId="password">
					<Form.Label></Form.Label>
					<Form.Control type="password" placeholder="Enter your password" value={password} onChange={(e) => setPassword(e.target.value)} required />
				</Form.Group>

				<Button variant="primary" type="submit">Submit</Button>
			</Form>
		</Container>
		</React.Fragment>
	)
}